# PIC-EXPRESS

Web application dédiée à la création de Plan d'Installation de Chantier à partir d'un plan cadastral.

Librairie utilisée : [Fabric.js](http://fabricjs.com/)

Contribution personnelle : 
- Outil de cotation en trois clics
- Undo, Redo, et Suppression d'éléments du plan
- Gestion de la mise à l'échelle automatique des éléments d'interfaces sur le plan
- Interface graphique

# Installation

Il faut d'abord installer l'ensemble des packages nécessaires.

``` npm install ```

*?? modifier le json ??*

# Démarrage de l'application

Pour lancer l'application Pic-Express, il suffit d'entrer :

``` npm start ```

# L'application

1.  La page d'accueil et de connexion

2.  Importer une image (plan cadsatral)

3.  Ajouter des éléments de chantier (grues, routes, ...)

4.  Sauvegarder son travail


