// AI18 >>>>>>>>>>>>>>>>>>>>>>>>

function getMapPath() {
    return localStorage.getItem('mapURI');
}

function getCanvasBackup() {
    if (localStorage.getItem('canvasJson') != null) {
        return JSON.parse(localStorage.getItem('canvasJson'));
    }
    return false;
}

function saveToDisk(mapPath, canvasBackup) {
    localStorage.setItem('mapURI', mapPath);
    localStorage.setItem('canvasJson', canvasBackup);
    // console.log(canvasBackup);
}
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<