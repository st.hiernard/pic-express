// Get the modal
var cartoucheModal = $("#cartoucheModal");

// Get the button that opens the modal
var btn = $("#downloadBtn");

// Get the <span> element that closes the modal
var span = $(".cancel")[0];

// When the user clicks on the button, open the modal
btn.onclick = function() {
    cartoucheModal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
// TODO : Ne fonctionne pas (Uncaught TypeError: Cannot set property 'onclick' of undefined)
// span.onclick = function() {
//     cartoucheModal.style.display = "none";
// }

// When the user clicks anywhere outside of the modal, close it
// TODO : Ne fonctionne pas (Uncaught TypeError: Cannot set property 'display' of undefined)
// window.onclick = function(event) {
//     if (event.target != cartoucheModal) {
//         cartoucheModal.style.display = "none";
//     }
// }

var cartoucheInfos = [];

var Cartouche = {
    formSubmitted: function () {
        if ($("#planName").val() != "" && $("#datePlan").val() != "" && $("#adressProject").val() != "" &&
            $("#planVersion").val() != "" && $("#format").val() != "" && $("#client").val() != "" &&
            $("#autheur").val() != "") {

            cartoucheInfos = [];
            cartoucheInfos["planName"] = $("#planName").val();
            cartoucheInfos["datePlan"] = $("#datePlan").val();
            cartoucheInfos["adressProject"] = $("#adressProject").val();
            cartoucheInfos["planVersion"] = $("#planVersion").val();
            cartoucheInfos["format"] = $("#format").val();
            cartoucheInfos["client"] = $("#client").val();
            cartoucheInfos["autheur"] = $("#autheur").val();

            $('#cartoucheModal').modal('hide');
        } else {
            alert("Vous devez entrer toutes les informations.")
        }
    },
    isReady: function() {
      if (Object.keys(cartoucheInfos).length == 0) {
          return false;
      }
      return true;
    },
    init: function (fabricCanvas) {
        fabricCanvas.setHeight(fabricCanvas.getHeight() + 110);

        let mainRect = new fabric.Rect({
            left: 0,
            top: canvas.getHeight() - 110,
            width: canvas.getWidth() - 2,
            height: 100,
            fill: 'rgb(255,255,255)',
            stroke: 'rgb(0,0,0)',
            strokeWidth: 2
        });


        let dateRect = new fabric.Rect({
            left: 0,
            top: canvas.getHeight() - 110,
            width: 225,
            height: 50,
            fill: 'rgb(255,255,255)',
            stroke: 'rgb(0,0,0)',
            strokeWidth: 2
        });

        let authorRect = new fabric.Rect({
            left: 0,
            top: canvas.getHeight() - 60,
            width: 225,
            height: 50,
            fill: 'rgb(255,255,255)',
            stroke: 'rgb(0,0,0)',
            strokeWidth: 2
        });

        let versionRect = new fabric.Rect({
            left: 225,
            top: canvas.getHeight() - 110,
            width: 225,
            height: 50,
            fill: 'rgb(255,255,255)',
            stroke: 'rgb(0,0,0)',
            strokeWidth: 2
        });

        let clientRect = new fabric.Rect({
            left: 225,
            top: canvas.getHeight() - 60,
            width: 225,
            height: 50,
            fill: 'rgb(255,255,255)',
            stroke: 'rgb(0,0,0)',
            strokeWidth: 2
        });

        fabricCanvas.add(mainRect);
        fabricCanvas.add(dateRect);
        fabricCanvas.add(authorRect);
        fabricCanvas.add(versionRect);
        fabricCanvas.add(clientRect);
    },
    setDateSection: function (fabricCanvas, date) {
        let dateObj = new fabric.Text("Le " + date, {
            left: 20,
            top: canvas.getHeight() - 95,
            fontFamily: 'Arial',
            fontSize: 20
        });
        fabricCanvas.add(dateObj);
    },
    setAuthorSection: function (fabricCanvas, author) {
        let authorTitleObj = new fabric.Text("Fait par :", {
            left: 20,
            top: canvas.getHeight() - 55,
            fontFamily: 'Arial',
            fontSize: 15
        });

        let authorObj = new fabric.Text(author, {
            left: 20,
            top: canvas.getHeight() - 35,
            fontFamily: 'Arial',
            fontSize: 15
        });

        fabricCanvas.add(authorTitleObj);
        fabricCanvas.add(authorObj);
    },
    setTitleSection: function (fabricCanvas, title) {
        let titleObj = new fabric.Text(title, {
            top: canvas.getHeight() - 85,
            left: 475,
            fontFamily: 'Arial',
            fontSize: 30
        });
        fabricCanvas.add(titleObj);
    },
    setClientSection: function (fabricCanvas, clientName) {
        let clientObj = new fabric.Text(clientName, {
            left: 245,
            top: canvas.getHeight() - 45,
            fontFamily: 'Arial',
            fontSize: 20
        });
        fabricCanvas.add(clientObj);
    },
    setVersionSection: function (fabricCanvas, version) {
        let versionObj = new fabric.Text(version, {
            left: 245,
            top: canvas.getHeight() - 95,
            fontFamily: 'Arial',
            fontSize: 20
        });
        fabricCanvas.add(versionObj);
    }
}
