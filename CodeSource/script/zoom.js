
var zoomValue = 1;


var Zoom = {
    init: function() {
        
        zoomValue = ($(document).width() - 700) / 1600;

        var initTopValue = 458 * zoomValue - 458;

        $("#mapAndCanvas-container").css("transform", "scale(" + zoomValue + ")");
        $("#mapAndCanvas-container").css("top", initTopValue + "px");

    },
    in:function() {
        zoomValue = zoomValue + 0.2;
        Zoom.setZoom();
    },
    out:function() {
        zoomValue = zoomValue - 0.2;
        Zoom.setZoom();
    },
    setZoom:function() {
        if (zoomValue < 0.2) {
            zoomValue = 0.2;
        } else if (zoomValue > 2) {
            zoomValue = 2;
        }
        $("#mapAndCanvas-container").css("transform", "scale(" + zoomValue + ")");
    }
}


$( document ).ready(function() {
    $('#mapAndCanvas-container').bind('mousewheel', function(e){
        if(e.originalEvent.wheelDelta / 120 > 0) {
            Zoom.out();
        }
        else{
            Zoom.in();
        }
    });
});