

function getScaleRealDistanceFromJson() {
    var canvasBackup = getCanvasBackup();
    return canvasBackup["scaleRealDistance"];
}

function getScaleDistanceFromJson() {
    var canvasBackup = getCanvasBackup();
    return canvasBackup["scaleDistance"];
}

function getCanvasJson() {
    var canvasBackup = getCanvasBackup();
    if (canvasBackup != false) {
        return canvasBackup["canvasJson"];
    } else {
        return false;
    }
}

// Lors du chargement de la page
window.onload = function(){ 
    document.querySelector("#loader-container").style.visibility = "visible"; 
    Zoom.init();
    $("#map").attr('src', getMapPath());
 }

// Lorsque le plan de masse a fini d'être chargé
function imgLoaded() {
    var canvasBackup = getCanvasJson();
    if (canvasBackup != false) {
        isNewProject = false;
    }
    
    $("#canvas").css("height", $("#map").css("height"));
    $("#canvas").css("width", $("#map").css("width"));
    
    initFabricjsCanvas();

    initLibrarySection(canvas, $('#mapAndCanvas-container')[0]);
    // initDragAndDrop(canvas, $('#mapAndCanvas-container')[0]);

    if (canvasBackup != false) {
        setScaleRealDistance(getScaleRealDistanceFromJson());
        setScaleDistance(getScaleDistanceFromJson());
        canvas.loadFromJSON(canvasBackup);
    }

    document.querySelector("aside").style.visibility = "visible";
    document.querySelector("#floating-section").style.visibility = "visible";
    document.querySelector("#mapAndCanvas-container").style.visibility = "visible";
    document.querySelector("#loader-container").style.display = "none"; 
}
