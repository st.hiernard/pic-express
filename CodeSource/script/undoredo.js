
//Les fonctions Undo et Redo sont fournis avec le fichier fabric-history.js
var UndoRedo = {
	
	//Annule la dernière modification du canvas
	undo:function(){
		if (startUsingTools()) {
			console.log("Undo");
			canvas.consoleLogStackStates();
			canvas.undo();
			canvas.consoleLogStackStates();
			console.log("\n");
		}
	},
	
	//Revient à l'état du canvas avant un undo
	redo:function(){
		if (startUsingTools()) {
			console.log("Redo");
			canvas.consoleLogStackStates();
			canvas.redo();
			canvas.consoleLogStackStates();
			console.log("\n");
		}
	},
	
	//Met en pause l'enregistrement des actions effectuées dans l'historique des fonctions undo et redo
	pause:function(){
		canvas._historyDispose();
	},
	
	//Continue l'enregistrement des actions effectuées dans l'historique des fonctions undo et redo
	resume:function(){
		canvas._historyUndispose();
	}
	
}

//Permet de faire undo et redo avec CTRL+Z et CTRL+Y
document.addEventListener('keydown', ({ keyCode, ctrlKey } = event) => {
      // Check Ctrl key is pressed.
      if (!ctrlKey) {
        return
      }
 
      // Check pressed button is Z - Ctrl+Z.
      if (keyCode === 90) {
		UndoRedo.undo();
      }
 
      // Check pressed button is Y - Ctrl+Y.
      if (keyCode === 89) {
        UndoRedo.redo();
      }
	  
    })



