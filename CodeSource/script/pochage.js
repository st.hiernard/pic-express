var mousedownPochage;
var mousemovePochage;

var Pochage = {
    activate:function() {
        var x = 0;
        var y = 0;
        var pochagePoints = [];
        var lines = [];
        var lineCounter = 0;
        var thickness = 3;

        function Point(x, y) {
            this.x = x;
            this.y = y;
        }

        // Permet de recuperer la position actuelle du curseur
        function setStartingPoint(options) {
            var pointer = canvas.getPointer(options.e);
            x = pointer.x;
            y = pointer.y;
        }
        
        canvas.on('mouse:down', mousedownPochage = function(options) {

            if (lineCounter == 0) {
                // Mise en pause de l'enregistrement du undo/redo
                UndoRedo.pause();
                CanvasSelection.makeEachObjectUnselectable();
            }

            if (options.button === 1) { // left click: ajout d'une nouvelle ligne au polygone
                runningTool = true;
                canvas.selection = false;
                setStartingPoint(options);
                pochagePoints.push(new Point(x, y));
                var points = [x, y, x, y];
                lines.push(new fabric.Line(points, {
                    strokeWidth: thickness,
                    selectable: false,
                    stroke: 'blue',
                    fill: 'blue',
                    originX: 'center',
                    originY: 'center'
                }));
                canvas.add(lines[lineCounter]);
                canvas.renderAll();
                lineCounter++;
            }
            else if (options.button === 3) { // right click: indique que le pochage est terminé

                // Suppression des lignes de construction
                lines.forEach(function(value, index, ar) {
                    canvas.remove(value);
                });

                // Creation du pochage
                pochagePoints.push(new Point(pochagePoints[0].x, pochagePoints[0].y))
                var polygon = new fabric.Polyline(pochagePoints, {
                    fill: getCurrentColorLight(),
                    strokeWidth: thickness,
                    stroke: getCurrentColor(),
                    hasControls: false,
                    lockMovementX: true,
                    lockMovementY: true,
                    lockScalingX: true,
                    loclScalingY: true,
                    lockUniScaling: true,
                    lockRotation: true
                });

                // Reprise de l'enregistrement du undo/redo
                UndoRedo.resume();

                // Ajout du pochage au canvas
                canvas.add(polygon);
                canvas.renderAll();
                canvas.selection = true;

                //reset
                pochagePoints = [];
                lines = [];
                lineCounter = 0;
                runningTool = false;
            }


        });

        // Prévisualisation de la ligne en cours de construction
        canvas.on('mouse:move', mousemovePochage = function(options) {
            if (lines[0] !== null && lines[0] !== undefined) {
                setStartingPoint(options);
                lines[lineCounter - 1].set({
                    x2: x,
                    y2: y
                });
                canvas.renderAll();
            }
        });

    },
    deactivate:function(){
        canvas.off('mouse:down', mousedownPochage);
        canvas.off('mouse:move', mousemovePochage);
        CanvasSelection.makeEachObjectSelectable();
    },
    switchActivateDeactivatePochage:function(){
        if(isPochageActivated){
            $("#pochageBtn").removeClass("selected").trigger('classChange');
            Pochage.deactivate();
            isPochageActivated = false;
        }else if (startUsingTools()) {
            $("#pochageBtn").addClass("selected").trigger('classChange');
            Pochage.activate();
            isPochageActivated = true;
        }
    }
}


$('#pochageBtn').on('classChange', colorPickerEventHandle);