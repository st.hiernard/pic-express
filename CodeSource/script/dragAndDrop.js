
var dAdNbItems = 0;
var dAdFabricCanvas, dAdHtmlContainer;


// Script englobant toutes les fonctions de Drag & Drop
function initDragAndDrop(fabricCanvas, htmlContainer) {
    $('.canvas-container').each(function(index) {
        // Sélectiond du canvas
        var canvasObject = $("canvas")[0];

        var positionX, positionY;

        // Décomposition du drag and drop en 5 fonctions DRAG et 1 fonction DROP
        // Fonction DRAG START
        function handleDragStart(e) {
            [].forEach.call(images, function(img) {
                img.classList.remove('img_dragging');
            });
            this.classList.add('img_dragging');

            var position = $(this).offset(); // position actuelle de l'image par rapport à la page
            // position du curseur sur l'image = position du curseur sur la page - position de l'image sur la page
            positionX = e.clientX - position.left; // coordonnée X du curseur sur l'image
            positionY = e.clientY - position.top; // coordonnée Y du curseur sur l'image
        }

        // Fonction DRAG OVER
        function handleDragOver(e) {
            if (e.preventDefault) {
                e.preventDefault();
            }
            e.dataTransfer.dropEffect = 'copy';
            return false;
        }

        // Fonction DRAG ENTER
        function handleDragEnter(e) {
            this.classList.add('over');
        }

        //Fonction DRAG LEAVE
        function handleDragLeave(e) {
            this.classList.remove('over');
        }

        // Fonction DROP : insertion de l'image qui a été déposée sur le cadastre
        function handleDrop(e) {
            e = e || window.event;
            if (e.preventDefault) {
                e.preventDefault();
            }
            if (e.stopPropagation) {
                e.stopPropagation();
            }
            var img = document.querySelector('.elementChantiers img.img_dragging');
            console.log('event: ', e);

            // récupération de la position du drop
            // var position = $(canvasObject).offset(); // position du canvas par rapport à la page
            // // position de l'image par rapport au canvas = position du curseur - (position du canvas sur la page + position du curseur sur l'image)
            // var x = e.clientX - (position.left + positionX);
            // var y = e.clientY - (position.top + positionY);
            var pointer = fabricCanvas.getPointer(e);
            var x = pointer.x - 40;
            var y = pointer.y - 30;

            console.log("x:" + x + " y:" + y);
            console.log("width:" + img.width + " height:" + img.height + " scale:" + Scale.getScale());

            fabric.Image.fromURL(img.src, function(oImg) {
                oImg.scaleToWidth(img.dataset.width/Scale.getScale());
                oImg.scaleToHeight(img.dataset.height/Scale.getScale());
                oImg.setOptions({
                    left: x,
                    top: y,
					lockScalingX: true, 
					loclScalingY: true, 
					lockUniScaling: true
                })

                fabricCanvas.add(oImg);
            });

            return false;
        }

        // Fonction DRAG END : une fois le drop effectué, l'image glissante disparaît
        function handleDragEnd(e) {
            [].forEach.call(images, function(img) {
                img.classList.remove('img_dragging');
            });
        }

        // utilisation des fonctions de Drag & Drop pour l'élément de chantier sélectionné
        var images = document.querySelectorAll('.elementChantiers img');
        [].forEach.call(images, function(img) {
            img.addEventListener('dragstart', handleDragStart, false);
            img.addEventListener('dragend', handleDragEnd, false);
        });
        htmlContainer.addEventListener('dragEnter', handleDragEnter, false);
        htmlContainer.addEventListener('dragOver', handleDragOver, false);
        htmlContainer.addEventListener('dragLeave', handleDragLeave, false);
        htmlContainer.addEventListener('drop', handleDrop, false);
    });
}


function handleImgItemLoaded() {
    dAdNbItems -= 1;
    console.log(dAdNbItems);
    if (dAdNbItems == 0) {
        initDragAndDrop(dAdFabricCanvas, dAdHtmlContainer);
    }
}


function getLibraryItemHtml(fileName, longueur, largeur) {
    var item = '<div class="itemCard">' +
                    '<div class="imgContainer elementChantiers">' +
                        '<img draggable="true" src="image/librairie/' + fileName + '" onload="handleImgItemLoaded();" data-height="'+longueur+'" data-width="'+largeur+'">' +
                    '</div>' +
                    '<p>' + fileName.substring(0, fileName.indexOf(".")).split("_").join(" ") + '</p>' +
                '</div>';
    return item;
}


function initLibrarySection(fabricCanvas, htmlContainer) {
    let url = window.location.origin + window.location.pathname.substring(0,window.location.pathname.lastIndexOf("/"));
    url +=  "/image/librairie/lstElementsDeChantier.json";
    console.log(url);
    $.getJSON( url, function( data ) 	{

        dAdFabricCanvas = fabricCanvas;
        dAdHtmlContainer = htmlContainer;
        dAdNbItems = data.elementsDeChantier.length;

        $.each(data.elementsDeChantier, function() {
            $('#librarySectionItems').append(getLibraryItemHtml(this["nom"], this["longueur"], this["largeur"]));    
            // console.log(getLibraryItemHtml(this));
        });

        // console.log(getLibraryItemHtml(data.elementsDeChantier[1]));
    });
}