
var Text;
Text = {   //Active la cotation
    activate: function () {
        canvas.selection = false;
        //Rend tous les objets du canvas non sélectionnables
        CanvasSelection.makeEachObjectUnselectable();
        canvas.on('mouse:down', createTextZone = function (event) {
            var pointer = canvas.getPointer(event.e);

            var textZone = new fabric.Textbox('Entrez votre texte', {
                fontSize: 24,
                textAlign: 'center',
                strokeWidth: 2,
                fontFamily: 'Arial'
            });
            textZone.top = (pointer.y + (pointer.y - textZone.height))/2;
            textZone.left = (pointer.x + (pointer.x - textZone.width))/2;

            canvas.on('text:changed', function (o) {
                var t1 = o.target;
                if (t1.width > t1.fixedWidth) {
                    t1.fontSize *= t1.fixedWidth / (t1.width + 1);
                    t1.width = t1.fixedWidth;
                }
            });
            //Continue l'enregistrement des actions dans l'historique de undo et redo
            UndoRedo.pause();

            UndoRedo.resume();

            //On ajoute la zone de texte
            canvas.add(textZone);
            Text.switchActivateDeactivateText();
        });

    }
    //Désactive la cotation
    , deactivate: function () {
        canvas.off('mouse:down', createTextZone );

        //Rend la multisélection du canvas possible
        canvas.selection = true;
        //Rend tous les objets du canvas sélectionnables
        CanvasSelection.makeEachObjectSelectable();

    }

    //Alterne entre l'activation et la désactivation de la cotation sur le canvas
    , switchActivateDeactivateText: function () {
        if (isTextActivated) {
            $("#textBtn")[0].style.color = "black";
            Text.deactivate();
            isTextActivated = false;
        } else if (startUsingTools()) {
            $("#textBtn")[0].style.color = "blue";
            Text.activate();
            isTextActivated = true;
        }
    }

};

