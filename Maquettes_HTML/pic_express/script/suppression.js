var Suppression = {
	//Supprime du canvas tous les élements sélectionnés
	removeSelection:function(){
		UndoRedo.pause();
		var cpt = canvas.getActiveObjects().length;
		canvas.getActiveObjects().forEach((obj) => {
			if(cpt==1){
				UndoRedo.resume();
			}
			canvas.remove(obj)
			cpt--;
		});
		canvas.discardActiveObject().renderAll()
	}
}

//Lance la fonction de suppression d'objets si la touche suppr est pressée
document.onkeydown = function(e) {
	switch (e.keyCode) {
		case 46:  /* delete key*/
          Suppression.removeSelection();
        break;
	}
}

