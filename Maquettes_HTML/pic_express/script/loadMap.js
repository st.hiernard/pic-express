// Pour ajouter une image à l'arrière plan de #canvas-container (redéfinition de l'event onload)
var img = new Image();

img.onload = function() {
    var f_img = new fabric.Image(img, {
        left: 300,
        opacity: 0.3,
        hasControls: false,
        lockMovementX: true,
        lockMovementY: true,
        lockScalingX: true,
        loclScalingY: true,
        lockUniScaling: true,
        lockRotation: true
    });

    f_img.scaleToHeight(canvas.height);

    /*canvas.setBackgroundImage(f_img, "");
    canvas.renderAll();*/

    canvas.add(f_img);
};

// Pour convertir un jpg en une image base 64
function convertToBase64() {
    var selectedFile = document.getElementById("inputFile").files;
    if (selectedFile.length > 0) {
        var fileToLoad = selectedFile[0];
        var fileReader = new FileReader();
        var base64;
        fileReader.onload = function(fileLoadedEvent) {
            base64 = fileLoadedEvent.target.result;
            img.src = base64;
        };
        fileReader.readAsDataURL(fileToLoad);
    }
}
