
var wheelZoom;

var Zoom = {
    in:function() {
        Zoom.zoomToPoint(canvas.width / 2, canvas.height / 2, canvas.getZoom() + 0.3);
    },
    out:function() {
        Zoom.zoomToPoint(canvas.width / 2, canvas.height / 2, canvas.getZoom() - 0.3);
    },
    zoomToPoint:function(x, y, zoomValue) {
        var minZoomValue = 1;
        var maxZoomValue = 3.6;

        if (zoomValue < minZoomValue) {
            zoomValue = minZoomValue;
        } else if (zoomValue > maxZoomValue) {
            zoomValue = maxZoomValue;
        }

        canvas.zoomToPoint(new fabric.Point(x, y), zoomValue);
    }
}


// /**
//  * Debounce functions for better performance
//  * (c) 2018 Chris Ferdinandi, MIT License, https://gomakethings.com
//  * @param  {Function} fn The function to debounce
//  */
// var debounce = function (fn) {
//
//     // Setup a timer
//     var timeout;
//
//     // Return a function to run debounced
//     return function () {
//
//         // Setup the arguments
//         var context = this;
//         var args = arguments;
//
//         // If there's a timer, cancel it
//         if (timeout) {
//             window.cancelAnimationFrame(timeout);
//         }
//
//         // Setup the new requestAnimationFrame()
//         timeout = window.requestAnimationFrame(function () {
//             fn.apply(context, args);
//         });
//
//     }
//
// };

// var zoomToPointDebounce = debounce(Zoom.zoomToPoint);

canvas.on('mouse:wheel', wheelZoom = function(opt) {
    var delta = opt.e.deltaY;
    var zoom = canvas.getZoom();

    Zoom.zoomToPoint(opt.e.offsetX, opt.e.offsetY, zoom + delta/100);

    // zoomToPointDebounce(opt.e.offsetX, opt.e.offsetY, canvas.getZoom() + opt.e.deltaY/100);

    opt.e.preventDefault();
    opt.e.stopPropagation();
});
