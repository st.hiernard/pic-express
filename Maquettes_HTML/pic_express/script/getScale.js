var scaleDistance = -1;

var Scale = 
{
	activateScaleCalculation:function(){
		
		if(startUsingTools()){
			
			//Notification de l'activation de l'outil
			isScaleCalculationActivated = true;
			
			//Empeche la sélection pendant la cotation (Cadre de sélection)
			canvas.selection = false;
			//Rend tous les objets du canvas non sélectionnables
			CanvasSelection.makeEachObjectUnselectable();
			
			//Bouton de mise à l'échelle devient bleu
			$("#scaleCalculationBtn")[0].style.color = "blue";

			var nbClics = "0";
			var startX;
			var startY;
			var endX;
			var endY;
			var isDown = false;
			
			//On retient quand le clic de souris est actif
			canvas.on('mouse:down', mousedownScaleCalculation = function(o){
				isDown = true;
			});
			
			//Quand le clic de souris est levé
			canvas.on('mouse:up', mouseupScaleCalculation = function(o){
				if(isDown){
					
					//Le clic de souris n'est plus actif
					isDown = false;
					
					if(nbClics=="1"){
						
						//On récupère le pointeur de souris
						var pointer = canvas.getPointer(o.e);
						endX = pointer.x;
						endY = pointer.y;
						
						//Calcul de la distance en pixel entre les 2 points cliqués
						scaleDistance = Math.sqrt(Math.pow(endX*1-startX*1, 2)+Math.pow(endY*1-startY*1, 2))
						
						nbClics = "0";
						
						//Rend la multisélection du canvas possible
						canvas.selection = true;
						//Rend tous les objets du canvas non sélectionnables
						CanvasSelection.makeEachObjectSelectable();
						
						//On stoppe le traitement du calcul de l'échelle
						canvas.off('mouse:down', mousedownScaleCalculation);
						canvas.off('mouse:up', mouseupScaleCalculation);
						
						//Bouton de mise à l'échelle devient bleu
						$("#scaleCalculationBtn")[0].style.color = "black";
						
						//Notification de la désactivation de l'outil
						isScaleCalculationActivated = false;
						
						console.log(scaleDistance);
						console.log(scaleRealDistance);
						console.log(Scale.getScale());
					}
					
					if(nbClics=="0"){
						
						//On récupère le pointeur de souris
						var pointer = canvas.getPointer(o.e);
						startX = pointer.x;
						startY = pointer.y;
						
						nbClics = "1";
					}
								
				}
			});
		}
	}
	
	//Renvoie la distance réelle que vaut 1 pixel
	,getScale:function(){
		return scaleRealDistance/scaleDistance;
	}
}