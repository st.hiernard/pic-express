
// Pour savoir quel outil est en cours d'utilisation
var isCotationActivated = false;
var isTextActivated = false;
var isPochageActivated = false;
var isClotureActivated = false;
var isScaleCalculationActivated = false;
var isMoveMapActivate = false;
var runningTool = false;

var minWidth = screen.width - 35;
var minHeight = screen.height - 280;

// On définit la taille du canvas en fonction de la résolution de l'écran.
var canvasContainerHTML = $('#canvas-container')[0];
canvasContainerHTML.width = minWidth;
canvasContainerHTML.height = minHeight;

// Objet canvas de frabricjs
var canvas = new fabric.Canvas("canvas-container", {
	perPixelTargetFind: true,
    targetFindTolerance: 30,
	hoverCursor: 'pointer',
	selection: true,
	//La propriété suivante permet que les éléments du canvas reste sur le même plan
	//Sans cela, quand plusieurs cotations se chevauchent, il est difficile de séléctionner le texte que l'on veut pour le bouger
	//https://jsfiddle.net/1vawp9gu/
	preserveObjectStacking: true,
	stopContextMenu: true,
	fireRightClick: true
});

//Empeche de bouger, changer de taille, rotationner une sélection d'objet du canvas si un des objets de la sélection possède ces propriétés
//Ce qui empêche les quotations d'êtres bougés si on les sélectionne tous ensemble
//https://jsfiddle.net/Lun395qv/
canvas.on('selection:created', ({ selected, target }) => {
	if (selected.some(obj => obj.lockMovementX)) {
		target.lockMovementX = true;
	}
  	if (selected.some(obj => obj.lockMovementY)) {
		target.lockMovementY = true;
	}
	if (selected.some(obj => obj.lockMovementY)) {
		target.lockScalingY = true;
	}
	if (selected.some(obj => obj.lockMovementX)) {
		target.lockScalingX = true;
	}
  	if (selected.some(obj => obj.lockMovementY)) {
		target.lockUniScaling = true;
	}
	if (selected.some(obj => obj.lockMovementY)) {
		target.lockRotation = true;
	}
})

//Distance réelle entre les deux points cliqués lors du calcul de l'échelle
var scaleRealDistance;
$("#scaleRealDistance").on('input',function(){ 
	 try {
		scaleRealDistance = $("#scaleRealDistance").val();
		scaleRealDistance = Number(scaleRealDistance);
		if(scaleRealDistance==null || scaleRealDistance<0 || (isNaN(scaleRealDistance))){
			scaleRealDistance = Number(1);
		}
	 }
	 catch(error) {
		scaleRealDistance = Number(1);
	 }
	 //console.log(scaleRealDistance);
	 if(scaleRealDistance!=1){
		$("#scaleRealDistance").val(scaleRealDistance);
	 }
});


var CanvasSelection = {
	//Fonction permettant de rendre tous les objects du canvas insélectionnables
	makeEachObjectUnselectable:function() {
		canvas.forEachObject(function(object){ 
			object.selectable = false; 
		});
	},
	//Fonction permettant de rendre tous les objects du canvas sélectionnables
	makeEachObjectSelectable:function() {
		canvas.forEachObject(function(object){ 
			object.selectable = true; 
		});
	}
}


/**
 * Permet de vérifier si un outil n'est pas en cours d'utilisation
 * pour éviter d'en activer un autre en même temps.
 */
function startUsingTools() {

	if (!isCotationActivated && !isTextActivated && !isPochageActivated && !isClotureActivated && !isMoveMapActivate && !isScaleCalculationActivated) {
		return true;
	} else if (!runningTool) {
		if (isCotationActivated) Cotation.switchActivateDeactivateCotation();
		if (isTextActivated) Text.switchActivateDeactivateText();
		if (isPochageActivated) Pochage.switchActivateDeactivatePochage();
		if (isClotureActivated) Cloture.switchActivateDeactivateCloture();
		if (isMoveMapActivate) MoveMap.clicked();

		return true;
	} else {
		return false;
	}
}
