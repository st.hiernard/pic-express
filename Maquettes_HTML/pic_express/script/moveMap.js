var mousedownMoveMap;
var mousemoveMoveMap;
var mouseupMoveMap;

var MoveMap = {
    start:function() {

        CanvasSelection.makeEachObjectUnselectable();

        canvas.on('mouse:down', mousedownMoveMap = function(opt) {
            var evt = opt.e;
            this.isDragging = true;
            this.selection = false;
            this.lastPosX = evt.clientX;
            this.lastPosY = evt.clientY;
        });
        canvas.on('mouse:move', mousemoveMoveMap = function(opt) {
            if (this.isDragging) {
                var evt = opt.e;
                this.viewportTransform[4] += evt.clientX - this.lastPosX;
                this.viewportTransform[5] += evt.clientY - this.lastPosY;
                this.requestRenderAll();
                this.lastPosX = evt.clientX;
                this.lastPosY = evt.clientY;
            }
        });
        canvas.on('mouse:up', mouseupMoveMap = function(opt) {
            this.isDragging = false;
            this.selection = true;
        });
    },
    end:function() {
        canvas.off('mouse:down', mousedownMoveMap);
        canvas.off('mouse:move', mousemoveMoveMap);
        canvas.off('mouse:up', mouseupMoveMap);
        // Pour résoudre le problème de l'emplacement des zones de sélection
        // qui sont incorrect après avoir déplacé le plan.
        Zoom.in();
        Zoom.out();
        CanvasSelection.makeEachObjectSelectable();
    },
    clicked:function () {
        if(isMoveMapActivate){
            $("#moveMapBtn")[0].style.color = "black";
            MoveMap.end();
            isMoveMapActivate = false;
        }else if (startUsingTools()) {
            $("#moveMapBtn")[0].style.color = "blue";
            MoveMap.start();
            isMoveMapActivate = true;
        }
    }
}