//Fabric-history permet de faire des undo et redo sur le canvas. Le script est à importer avant la création du canvas
//Modifié par Hiernard Stéphane

/**
 * Override the initialize function for the _historyInit();
 */
fabric.Canvas.prototype.initialize = (function(originalFn) {
  return function(...args) {
    originalFn.call(this, ...args);
    this._historyInit();
    return this;
  };
})(fabric.Canvas.prototype.initialize);

/**
 * Override the dispose function for the _historyDispose();
 */
fabric.Canvas.prototype.dispose = (function(originalFn) {
  return function(...args) {
    originalFn.call(this, ...args);
    this._historyDispose();
    return this;
  };
})(fabric.Canvas.prototype.dispose);

/**
 * Returns current state of the string of the canvas
 */
fabric.Canvas.prototype._historyNext = function () {
  //Argument de toJSON = propriétés sauvegardées en plus  pour les objects du canvas
  return JSON.stringify(this.toJSON(['lockMovementX', 'lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY', 'lockUniScaling', 'hasControls', 'state']));
}

/**
 * Returns an object with fabricjs event mappings
 */
fabric.Canvas.prototype._historyEvents = function() {
  return {
    "object:added": this._historySaveAction,
    "object:removed": this._historySaveAction,
    "object:modified": this._historySaveAction,
    "object:skewing": this._historySaveAction
  }
}

/**
 * Initialization of the plugin
 */
fabric.Canvas.prototype._historyInit = function () {
  this.historyUndo = [];
  this.historyRedo = [];
  this.historyNextState = this._historyNext();
  this.state = 0; //Numéro de l'état du canvas
  
  undoMaxLength = 10; //Taille maximale de la liste historyUndo
  
  //récupère l'event push d'un array
  var eventify = function(arr, callback) {
    arr.push = function(e) {
        Array.prototype.push.call(arr, e);
        callback(arr);
    };
  };

  //Pose un listener sur la liste historyUndo et supprime le 1er élément de la liste si on dépasse plus de UndoMaxLength élements
  eventify(this.historyUndo, function(newArray) {
    if(newArray.length > undoMaxLength){
		newArray.shift();
	}
  });
  
  this.on(this._historyEvents());
}

/**
 * Remove the custom event listeners
 */
fabric.Canvas.prototype._historyDispose = function () {
  this.off(this._historyEvents())
}

/**
 * Add the custom event listeners (ajouté par Stéphane Hiernard)
 */
fabric.Canvas.prototype._historyUndispose = function () {
  this.on(this._historyEvents())
}

/**
 * It pushes the state of the canvas into history stack
 */
fabric.Canvas.prototype._historySaveAction = function () {

  if (this.historyProcessing)
    return;
	
  //Redo Stack reset after a not undo event
  this.historyRedo = [];

  //console.log("Evenement detecte : push sur la pile Undo du canvas d'etat : "+this.state);
	
  const json = this.historyNextState;
  this.historyUndo.push(json);
  
  //this.consoleLogStackStates();
  
  this.state++;
  
  this.historyNextState = this._historyNext();
  
  
  
  this.fire('history:append', { json: json });
}

/**
 * Undo to latest history. 
 * Pop the latest state of the history. Re-render.
 * Also, pushes into redo history.
 */
fabric.Canvas.prototype.undo = function () {
  // The undo process will render the new states of the objects
  // Therefore, object:added and object:modified events will triggered again
  // To ignore those events, we are setting a flag.
  this.historyProcessing = true;

  const history = this.historyUndo.pop();
  if (history) {
    // Push the current state to the redo history
    this.historyRedo.push(this._historyNext());
	
	//console.log("Undo active");
	//console.log("Push sur la pile Redo du canvas d'etat : "+this.state);
	
    this.loadFromJSON(history).renderAll();
	this.historyNextState = this._historyNext();
	
	//console.log("Pop de la pile Undo et chargement du canvas d'etat : "+this.state);
	
	//this.consoleLogStackStates();
	
    this.fire('history:undo');
  }

  this.historyProcessing = false;
}

/**
 * Redo to latest undo history.
 */
fabric.Canvas.prototype.redo = function () {
  // The undo process will render the new states of the objects
  // Therefore, object:added and object:modified events will triggered again
  // To ignore those events, we are setting a flag.
  this.historyProcessing = true;
  const history = this.historyRedo.pop();
  if (history) {
    // Every redo action is actually a new action to the undo history
    this.historyUndo.push(this._historyNext());
	
	//console.log("Redo active");
	//console.log("Push sur la pile Undo du canvas d'etat : "+this.state);
    
    this.loadFromJSON(history).renderAll();
	this.historyNextState = this._historyNext();
	
	//console.log("Pop de la pile Redo et chargement du canvas d'etat : "+this.state);
	
	//this.consoleLogStackStates();
	
    this.fire('history:redo');
  }

  this.historyProcessing = false;
}

/**
 * Clear undo and redo history stacks
 */
fabric.Canvas.prototype.clearHistory = function() {
  this.historyUndo = [];
  this.historyRedo = [];
  this.fire('history:clear');
}

/**
 * Affiche sur la console les piles de undo et redo (utile pour le debug)
 */
fabric.Canvas.prototype.consoleLogStackStates = function() {
  var historyUndoState = [];
  var historyRedoState = [];
  var canvasState = new fabric.Canvas();
  canvasState._historyDispose();
  this.historyUndo.forEach(element => {canvasState.loadFromJSON(element); historyUndoState.push(canvasState.state);});
  this.historyRedo.forEach(element => {canvasState.loadFromJSON(element); historyRedoState.push(canvasState.state);});
  console.log("UndoStack : "+historyUndoState);
  console.log("RedoStack : "+historyRedoState);
}