var mousedownCloture;
var mousemoveCloture;

var Cloture = {
    activate:function() {
        var x = 0;
        var y = 0;
        var roof = null;
        var roofPoints = [];
        var lines = [];
        var lineCounter = 0;
        var drawingObject = {};
        var thickness = 1.5;

        drawingObject.type = "";
        drawingObject.background = "";
        drawingObject.border = "";

        beginCloture();

        function getCurrentColor() {
            var color = $('#colorPicker').val();
            return 'rgba(' + parseInt(color.slice(-6, -4), 16) + ',' + parseInt(color.slice(-4, -2), 16) + ',' + parseInt(color.slice(-2), 16) + ',1)';
        }

        function getCurrentColorLight() {
            var color = $('#colorPicker').val();
            return 'rgba(' + parseInt(color.slice(-6, -4), 16) + ',' + parseInt(color.slice(-4, -2), 16) + ',' + parseInt(color.slice(-2), 16) + ',0.4)';
        }

        function Point(x, y) {
            this.x = x;
            this.y = y;
        }

        function beginCloture() {
            UndoRedo.pause();
            CanvasSelection.makeEachObjectUnselectable();
            if (drawingObject.type == "roof") {
                drawingObject.type = "";
                lines.forEach(function(value, index, ar) {
                    canvas.remove(value);
                });

                roof = makeRoofCloture(roofPoints);

                canvas.add(roof);
                canvas.renderAll();
            } else {
                drawingObject.type = "roof"; // roof type
            }
        }

        function setStartingPoint(options) {
            var pointer = canvas.getPointer(options.e);
            x = pointer.x;
            y = pointer.y;
        }

        function makeRoofCloture(roofPoints) {
            var roof = new fabric.Polyline(roofPoints, {
                fill: 'rgba(0,0,0,0)',
                strokeDashArray: [5, 5],
                strokeWidth: thickness,
                stroke: 'red',
                hasControls: false,
                lockMovementX: true,
                lockMovementY: true,
                lockScalingX: true,
                loclScalingY: true,
                lockUniScaling: true,
                lockRotation: true
            });
            return roof;
        }

        canvas.on('mouse:down', mousedownCloture = function(options) {

            if (options.button === 1) { // left click
                if (drawingObject.type == "roof") {
                    runningTool = true;
                    canvas.selection = false;
                    setStartingPoint(options); // set x,y
                    roofPoints.push(new Point(x, y));
                    var points = [x, y, x, y];
                    lines.push(new fabric.Line(points, {
                        strokeWidth: thickness,
                        selectable: false,
                        stroke: 'blue',
                        fill: 'blue',
                        originX: 'center',
                        originY: 'center'
                    }));
                    canvas.add(lines[lineCounter]);
                    lineCounter++;
                    canvas.renderAll();
                    canvas.on('mouse:up', function(options) {
                        canvas.selection = true;
                    });
                }
            }
            else if (options.button === 3) { // right click
                drawingObject.type = "";
                lines.forEach(function(value, index, ar) {
                    canvas.remove(value);
                });
                roof = makeRoofCloture(roofPoints);

                UndoRedo.resume();
                canvas.add(roof);
                canvas.renderAll();

                //reset
                roofPoints = [];
                lines = [];
                lineCounter = 0;

                beginCloture();

                runningTool = false;
            }


        });

        canvas.on('mouse:move', mousemoveCloture = function(options) {
            if (lines[0] !== null && lines[0] !== undefined && drawingObject.type == "roof") {
                setStartingPoint(options);
                lines[lineCounter - 1].set({
                    x2: x,
                    y2: y
                });
                canvas.renderAll();
            }
        });

    },
    deactivate:function(){
        canvas.off('mouse:down', mousedownCloture);
        canvas.off('mouse:move', mousemoveCloture);
        CanvasSelection.makeEachObjectSelectable();
    },
    switchActivateDeactivateCloture:function(){
        if(isClotureActivated){
            $("#clotureBtn")[0].style.color = "black";
            Cloture.deactivate();
            isClotureActivated = false;
        }else  if (startUsingTools()) {
            $("#clotureBtn")[0].style.color = "blue";
            Cloture.activate();
            isClotureActivated = true;
        }
    }
}