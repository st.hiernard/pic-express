
//Les fonctions Undo et Redo sont fournis avec le fichier fabric-history.js
var UndoRedo = {
	
	//Annule la dernière modification du canvas
	undo:function(){
		canvas.undo();
	},
	
	//Revient à l'état du canvas avant un undo
	redo:function(){
		canvas.redo();
	},
	
	//Met en pause l'enregistrement des actions effectuées dans l'historique des fonctions undo et redo
	pause:function(){
		canvas._historyDispose();
	},
	
	//Continue l'enregistrement des actions effectuées dans l'historique des fonctions undo et redo
	resume:function(){
		canvas._historyUndispose();
	}
	
}

//Permet de faire undo et redo avec CTRL+Z et CTRL+Y
document.addEventListener('keydown', ({ keyCode, ctrlKey } = event) => {
      // Check Ctrl key is pressed.
      if (!ctrlKey) {
        return
      }
 
      // Check pressed button is Z - Ctrl+Z.
      if (keyCode === 90) {
		UndoRedo.undo();
      }
 
      // Check pressed button is Y - Ctrl+Y.
      if (keyCode === 89) {
        UndoRedo.redo();
      }
	  
    })



