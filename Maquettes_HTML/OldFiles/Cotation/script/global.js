// On définit la taille du canvas en fonction de la résolution de l'écran.
var canvasContainerHTML = $('#canvas-container')[0];
canvasContainerHTML.width = screen.width - 35;
canvasContainerHTML.height = screen.height;

// Objet canvas de frabricjs
var canvas = new fabric.Canvas("canvas-container", {
	perPixelTargetFind: true,
    targetFindTolerance: 30,
	hoverCursor: 'pointer',
	selection: true,
	//La propriété suivante permet que les éléments du canvas reste sur le même plan
	//Sans cela, quand plusieurs cotations se chevauchent, il est difficile de séléctionner le texte que l'on veut pour le bouger
	//https://jsfiddle.net/1vawp9gu/
	preserveObjectStacking: true
});

//Empeche de bouger, changer de taille, rotationner une sélection d'objet du canvas si un des objets de la sélection possède ces propriétés
//Ce qui empêche les quotations d'êtres bougés si on les sélectionne tous ensemble
//https://jsfiddle.net/Lun395qv/
canvas.on('selection:created', ({ selected, target }) => {
	if (selected.some(obj => obj.lockMovementX)) {
		target.lockMovementX = true;
	}
  	if (selected.some(obj => obj.lockMovementY)) {
		target.lockMovementY = true;
	}
	if (selected.some(obj => obj.lockMovementY)) {
		target.lockScalingY = true;
	}
	if (selected.some(obj => obj.lockMovementX)) {
		target.lockScalingX = true;
	}
  	if (selected.some(obj => obj.lockMovementY)) {
		target.lockUniScaling = true;
	}
	if (selected.some(obj => obj.lockMovementY)) {
		target.lockRotation = true;
	}
})

//Echelle du canvas, utilisée pour les cctations
var scale;
$("#scale").on('input',function(){ 
	 try {
		scale = $("#scale").val();
		scale = Number(scale);
		if(scale==null || scale<0 || (isNaN(scale))){
			scale = Number(1);
		}
	 }
	 catch(error) {
		scale = Number(1);
	 }
	 //console.log(scale);
	 if(scale!=1){
		$("#scale").val(scale);
	 }
});


var CanvasSelection = {
	//Fonction permettant de rendre tous les objects du canvas insélectionnables
	makeEachObjectUnselectable:function() {
		canvas.forEachObject(function(object){ 
			object.selectable = false; 
		});
	},
	//Fonction permettant de rendre tous les objects du canvas sélectionnables
	makeEachObjectSelectable:function() {
		canvas.forEachObject(function(object){ 
			object.selectable = true; 
		});
	}
}