// Pour ajouter une image à l'arrière plan de #canvas-container (redéfinition de l'event onload)
var img = new Image();
img.onload = function() {
    var f_img = new fabric.Image(img);
    f_img.opacity = 0.3;
    f_img.scaleToWidth(canvas.width);
    f_img.scaleToHeight(canvas.height);
    canvas.setBackgroundImage(f_img);
    canvas.renderAll();
};

// Pour convertir un pdf en une image base 64 et la mettre à l'arrière plan de #canvas-container
function convertToBase64() {
    var selectedFile = document.getElementById("inputFile").files;
    if (selectedFile.length > 0) {
        var fileToLoad = selectedFile[0];
        var fileReader = new FileReader();
        var base64;
        fileReader.onload = function(fileLoadedEvent) {
            base64 = fileLoadedEvent.target.result;
            img.src = base64;
        };
        fileReader.readAsDataURL(fileToLoad);
    }
}