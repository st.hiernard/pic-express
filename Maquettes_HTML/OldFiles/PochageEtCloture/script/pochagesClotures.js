var isPochageClotureActivated = false;
var mousedownPochageCloture;
var mousemovePochageCloture;
var dblclickPochageCloture;

var PochageCloture = {
	start:function(pochageOrCloture) {
        var pochage = false;
        var cloture = false;
        var x = 0;
        var y = 0;
        var roof = null;
        var roofPoints = [];
        var lines = [];
        var lineCounter = 0;
        var drawingObject = {};

        drawingObject.type = "";
        drawingObject.background = "";
        drawingObject.border = "";

        if (pochageOrCloture == "pochage") {
            console.log("pochage");
            pochage = true;
            cloture = false;
            beginPochageOrCloture();
        }
        else if (pochageOrCloture == "cloture") {
            console.log("cloture");
            pochage = false;
            cloture = true;
            beginPochageOrCloture();
        }

        function getCurrentColor() {
            var color = $('#colorPicker').val();
            return 'rgba(' + parseInt(color.slice(-6, -4), 16) + ',' + parseInt(color.slice(-4, -2), 16) + ',' + parseInt(color.slice(-2), 16) + ',1)';
        }

        function getCurrentColorLight() {
            var color = $('#colorPicker').val();
            return 'rgba(' + parseInt(color.slice(-6, -4), 16) + ',' + parseInt(color.slice(-4, -2), 16) + ',' + parseInt(color.slice(-2), 16) + ',0.4)';
        }

        function Point(x, y) {
            this.x = x;
            this.y = y;
        }

        function beginPochageOrCloture() {
            if (drawingObject.type == "roof") {
                drawingObject.type = "";
                lines.forEach(function(value, index, ar) {
                    canvas.remove(value);
                });
                
                if (pochage) {
                    roof = makeRoofPochage(roofPoints);
                } else if (cloture) {
                    roof = makeRoofCloture(roofPoints);
                }

                canvas.add(roof);
                canvas.renderAll();
            } else {
                drawingObject.type = "roof"; // roof type
            }
        }

        function setStartingPoint(options) {
            var offset = $('#canvas-container').offset();
            x = options.e.pageX - offset.left;
            y = options.e.pageY - offset.top;
        }

        function makeRoofPochage(roofPoints) {
            roofPoints.push(new Point(roofPoints[0].x, roofPoints[0].y))
            var roof = new fabric.Polyline(roofPoints, {
                fill: getCurrentColorLight(),
                strokeWidth: 3,
                stroke: getCurrentColor(),
                hasControls: false,
            });
            return roof;
        }

        function makeRoofCloture(roofPoints) {
            var roof = new fabric.Polyline(roofPoints, {
                fill: 'rgba(0,0,0,0)',
                strokeDashArray: [5, 5],
                strokeWidth: 2,
                stroke: 'red',
                hasControls: false
            });
            return roof;
        }

        canvas.on('mouse:down', mousedownPochageCloture = function(options) {
            if (drawingObject.type == "roof") {
                canvas.selection = false;
                setStartingPoint(options); // set x,y
                roofPoints.push(new Point(x, y));
                var points = [x, y, x, y];
                lines.push(new fabric.Line(points, {
                    strokeWidth: 3,
                    selectable: false,
                    stroke: 'blue',
                    fill: 'blue',
                    originX: 'center',
                    originY: 'center'
                }));
                canvas.add(lines[lineCounter]);
                lineCounter++;
                canvas.renderAll();
                canvas.on('mouse:up', function(options) {
                    canvas.selection = true;
                });
            }
        });

        canvas.on('mouse:move', mousemovePochageCloture = function(options) {
            if (lines[0] !== null && lines[0] !== undefined && drawingObject.type == "roof") {
                setStartingPoint(options);
                lines[lineCounter - 1].set({
                    x2: x,
                    y2: y
                });
                canvas.renderAll();
            }
        });

        canvas.on('mouse:dblclick', dblclickPochageCloture = function(options) {
            drawingObject.type = "";
            lines.forEach(function(value, index, ar) {
                canvas.remove(value);
            });
            if (pochage) {
                roof = makeRoofPochage(roofPoints);
            } else if (cloture) {
                roof = makeRoofCloture(roofPoints);
            }

            canvas.add(roof);
            canvas.renderAll();
            
            //reset
            roofPoints = [];
            lines = [];
            lineCounter = 0;

            canvas.off('mouse:down', mousedownPochageCloture);
            canvas.off('mouse:move', mousemovePochageCloture);
            canvas.off('mouse:dblclick', dblclickPochageCloture);
        });
    }
}