// On définit la taille du canvas en fonction de la résolution de l'écran.
var canvasContainerHTML = $('#canvas-container')[0];
canvasContainerHTML.width = screen.width - 35;
canvasContainerHTML.height = screen.height;

// Objet canvas de frabricjs
var canvas = new fabric.Canvas("canvas-container", {});