let current_project = null;

$(window).on('load', async function() {
  let filter = '';
  init_bd();
  let projects = await get_projects();
  let filteredProjects = projects;
  $('#search').change(function() {
    filter = $('#search').val();
    if (filter) {
      filteredProjects = projects.filter(p => p[1].name.includes(filter))
    } else {
      filteredProjects = projects;
    }
    render_projects(filteredProjects);
  });
  // Add project
  $('#add-project').click(function() {
    $('#PIC-modal-add').removeClass('PIC-modal--hidden');
    $('#PIC-modal-add').click(function(event) {
      if (this === event.target) {
        $(this).addClass('PIC-modal--hidden');
      }
    })
  });

  $('#PIC-modal-add form').on('submit', async function(event) {
    event.stopPropagation();
    event.preventDefault();
    // const input = $(this).find('input#project-plan');
    const input = document.querySelector('#PIC-modal-add input#project-plan');
    const project = {
      name: $(this).find('input#project-name').val(),
      user: {
        name: $(this).find('input#user-name').val(),
        surname: $(this).find('input#user-surname').val(),
      },
      scale: $(this).find('input#project-scale').val(),
    };
    const file = input.files[0];
    const reader = new FileReader();
    await new Promise(resolve => {
      reader.addEventListener('load', event => {
        project['plan'] = event.target.result;
        resolve(true);
      });
      reader.readAsDataURL(file);
    });
    // console.log('project', project);
    await add_project(project);
    projects = await get_projects();
    $('#PIC-modal-add').addClass('PIC-modal--hidden');
    render_projects(projects);
  });
  // Import project
  $('#import-project').click(function() {
    $('#PIC-modal-import').removeClass('PIC-modal--hidden');
    $('#PIC-modal-import').click(function(event) {
      if (this === event.target) {
        $(this).addClass('PIC-modal--hidden');
      }
    })
  });
  $('#PIC-modal-import form').on('submit', async function(event) {
    event.stopPropagation();
    event.preventDefault();
    /*
    const project = {
      name: $(this).find('input#project-name').val(),
      user: {
        name: $(this).find('input#user-name').val(),
        surname: $(this).find('input#user-surname').val(),
      },
      plan: $(this).find('input#project-plan').val(),
      scale: $(this).find('input#project-scale').val(),
    };
    await add_project(project);
    projects = await get_projects();
    */
    $('#PIC-modal-add').addClass('PIC-modal--hidden');
    render_projects(projects);
  });

  $('.PIC-tool header a').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    document.location.reload();
  });
  render_projects(projects);

});

function render_projects(projects) {
  console.log(projects);
  $('.PIC-body > div:not(.constant)').remove();
  projects.forEach(project => {
    $('.PIC-body').append(`
    <div class="PIC-body-unit col-md-2" id="${project[0]}">
      <div>
        ${project[1].name}
      </div>
      <span class="close">&times;</span>
    </div>
    `);
    $(`.PIC-body #${project[0]} .close`).click(async function() {
      await delete_project(project[0]); 
      $(`.PIC-body #${project[0]}`).remove();
    });
    $(`.PIC-body #${project[0]} > div`).click(async function(e) {
      current_project = project;
      loadPicTool();
    });
  });
}