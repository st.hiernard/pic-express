function init_bd() {
  // Sur la ligne suivante, vous devez inclure les préfixes des implémentations que vous souhaitez tester.
  window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
  // N'UTILISEZ PAS "var indexedDB = ..." si vous n'êtes pas dans une fonction.
  // De plus, vous pourriez avoir besoin de réferences à des objets window.IDB*:
  window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
  window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange
  // (Mozilla n'a jamais préfixé ces objets, donc nous n'avons pas besoin de window.mozIDB*)
  if (!window.indexedDB) {
    window.alert("Votre navigateur ne supporte pas une version stable d'IndexedDB. Quelques fonctionnalités ne seront pas disponibles.")
  }
}

async function get_bd() {
  return new Promise(resolve => {
    const request = window.indexedDB.open("PIC-Express", 1);
    request.onerror = function(event) {
      alert("Pourquoi ne permettez-vous pas à ma web app d'utiliser IndexedDB?!");
    };
    request.onsuccess = function(event) {
      db = event.target.result;
      resolve(db);
    };
    request.onupgradeneeded = function(event) { 
      const db = event.target.result;
    
      // Crée un objet de stockage pour cette base de données
      const objectStore = db.createObjectStore("project", { autoIncrement: true });
      console.log('Database created');
    };
  });
}

function get_projects() {
  return new Promise(async (resolve, reject) => {

    const db = await get_bd();
    const result = [];
    db.transaction("project").objectStore("project").openCursor().onsuccess = function(event) {
      var cursor = event.target.result;
      if (cursor) {
        result.push([cursor.key, cursor.value]);
        cursor.continue();
      } else {
        resolve(result);
      }
    };
  });
}

function add_project(project) {
  return new Promise(async function(resolve, reject){
    const db = await get_bd();
    const transaction = db.transaction("project", "readwrite");
    /*transaction.onsuccess = function() {
      resolve(true);
    }*/
    transaction.objectStore("project").add(project).onsuccess = function() {
      resolve(true);
    };
  });
}

function delete_project(key) {
  return new Promise(async function(resolve, reject){
    const db = await get_bd();
    const transaction = db.transaction("project", "readwrite");
    /*transaction.onsuccess = function() {
      resolve(true);
    }*/
    transaction.objectStore("project").delete(key).onsuccess = function() {
      resolve(true);
    };
  });
}

function update_project(whole) {
  const key = whole[0];
  const project = whole[1];
  console.log(project);
  return new Promise(async function(resolve, reject){
    const db = await get_bd();
    // const objectStore = db.transaction("project", "readwrite").objectStore("project");
    /*transaction.onsuccess = function() {
      resolve(true);
    }*/
    const transaction = db.transaction("project", "readwrite");
    const objectStore = transaction.objectStore('project');
    const request = objectStore.put(project, key);
    request.onsuccess = function() {
      resolve(true);
    };
  });
}