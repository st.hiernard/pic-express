var scaleMouseLeftClickIsDown = false;

var Scale = 
{
	activateScaleCalculation:function(){
		
		if(startUsingTools()){
			
			//Notification de l'activation de l'outil
			isScaleCalculationActivated = true;
			runningTool = true;

			// Affichage d'un message dans le header pour indiquer que la calibration de la cotation est en cours
			$("#calibration-msg")[0].hidden = false;
			
			//Empeche la sélection pendant la cotation (Cadre de sélection)
			canvas.selection = false;
			//Rend tous les objets du canvas non sélectionnables
			CanvasSelection.makeEachObjectUnselectable();

			var nbClics = "0";
			var startX;
			var startY;
			var endX;
			var endY;
			
			//On retient quand le clic de souris est actif
			canvas.on('mouse:down', mousedownScaleCalculation = function(o){
				scaleMouseLeftClickIsDown = true;
			});
			
			//Quand le clic de souris est levé
			canvas.on('mouse:up', mouseupScaleCalculation = function(o){
				if(scaleMouseLeftClickIsDown){
					
					//Le clic de souris n'est plus actif
					scaleMouseLeftClickIsDown = false;
					
					if(nbClics=="1"){
						
						//On récupère le pointeur de souris
						var pointer = canvas.getPointer(o.e);
						endX = pointer.x;
						endY = pointer.y;
						
						//Calcul de la distance en pixel entre les 2 points cliqués
						scaleDistance = Math.sqrt(Math.pow(endX*1-startX*1, 2)+Math.pow(endY*1-startY*1, 2))
						
						nbClics = "2";
						
						//Rend la multisélection du canvas possible
						canvas.selection = true;
						//Rend tous les objets du canvas non sélectionnables
						CanvasSelection.makeEachObjectSelectable();
						
						//On stoppe le traitement du calcul de l'échelle
						canvas.off('mouse:down', mousedownScaleCalculation);
						canvas.off('mouse:up', mouseupScaleCalculation);
						
						//Notification de la désactivation de l'outil
						isScaleCalculationActivated = false;
						runningTool= false;
						
						secondAlert();

						//On retire le message dans le header
						$("#calibration-msg")[0].hidden = true;
						
						console.log(scaleDistance);
						console.log(scaleRealDistance);
						console.log(Scale.getScale());
					}
					
					if(nbClics=="0"){
						
						//On récupère le pointeur de souris
						var pointer = canvas.getPointer(o.e);
						startX = pointer.x;
						startY = pointer.y;
						
						nbClics = "1";
						
						firstAlert();
					}
							
				}
			});
		}
	}
	
	//Renvoie la distance réelle que vaut 1 pixel
	,getScale:function(){
		return scaleRealDistance/scaleDistance;
	}
}

function firstAlert(){
	$("#first-click-scale-alert").show();
	$("#first-click-scale-alert").fadeTo(2000, 500).slideUp(500, function(){
		$("#first-click-scale-alert").slideUp(500);
	});
}

function secondAlert(){
	$("#first-click-scale-alert").hide();
	$("#second-click-scale-alert").show();
	$("#second-click-scale-alert").fadeTo(2000, 500).slideUp(500, function(){
		$("#second-click-scale-alert").slideUp(500);
	});
}