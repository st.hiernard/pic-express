
var Save = {
    saveOnly: function() {
        var projectJsonBackup = {};
        projectJsonBackup["scaleRealDistance"] = scaleRealDistance;
        projectJsonBackup["scaleDistance"] = scaleDistance;
        projectJsonBackup["canvasJson"] = canvas.toJSON(['lockMovementX', 'lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY', 'lockUniScaling', 'hasControls', 'state']);

        saveToDisk($("#map").attr('src'), JSON.stringify(projectJsonBackup));
    },
    saveAndNotify:function() {
        Save.saveOnly();
        alert("L'état actuel de votre PIC a bien été sauvegardé.");
    },
    downloadPIC: function() {
        if (Cartouche.isReady()) {
            // Affichage d'un message d'explication et de chargement
            $("#download-msg")[0].hidden = false;
            Save.saveOnly();
            UndoRedo.pause();

            // Pour ajouter une image à l'arrière plan de #canvas-container (redéfinition de l'event onload)
            var img = new Image();
            img.onload = function() {
                var f_img = new fabric.Image(img);
                f_img.scaleToHeight(canvas.height);
                canvas.setBackgroundImage(f_img, "", {
                    opacity: 0.3
                });

                Cartouche.init(canvas);
                Cartouche.setDateSection(canvas, cartoucheInfos["datePlan"]);
                Cartouche.setAuthorSection(canvas, cartoucheInfos["autheur"]);
                Cartouche.setTitleSection(canvas, cartoucheInfos["planName"]);
                Cartouche.setClientSection(canvas, cartoucheInfos["client"]);
                Cartouche.setVersionSection(canvas, cartoucheInfos["planVersion"]);

                canvas.renderAll();
                let ctx = canvas.getContext();
                ctx.imageSmoothingEnabled = false;

                var map = canvas.toDataURL({
                    format: 'png',
                    multiplier: 5
                });

                // On retire le message de chargement
                $("#download-msg")[0].hidden = true;

                let download = document.createElement('a');
                download.href = map;
                download.download = 'pic.png';
                download.click();

                //location.reload();
                canvas.loadFromJSON(getCanvasJson());
                UndoRedo.resume();

            };
            img.src = $("#map").attr('src');
        } else {
            alert("Vous devez éditer le cartouche avant de télécharger le PIC.")
        }
    }
}

