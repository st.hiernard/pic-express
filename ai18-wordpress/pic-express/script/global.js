var isNewProject = true;

// Pour savoir quel outil est en cours d'utilisation
var isCotationActivated = false;
var isTextActivated = false;
var isPochageActivated = false;
var isClotureActivated = false;
var isScaleCalculationActivated = false;
var isMoveMapActivate = false;
var runningTool = false;

var canvasContainerHTML;

// Objet canvas de frabricjs
var canvas;
var CanvasSelection;

// Echelle
var scaleRealDistance;
var scaleDistance = -1;




function initFabricjsCanvas() {

    canvasContainerHTML = $('#canvas')[0];
    canvasContainerHTML.width = $("#map").css("width").replace('px', '');
    canvasContainerHTML.height = $("#map").css("height").replace('px', '');

    canvas = new fabric.Canvas("canvas", {
        perPixelTargetFind: true,
        targetFindTolerance: 30,
        hoverCursor: 'pointer',
        selection: true,
        //La propriété suivante permet que les éléments du canvas reste sur le même plan
        //Sans cela, quand plusieurs cotations se chevauchent, il est difficile de séléctionner le texte que l'on veut pour le bouger
        //https://jsfiddle.net/1vawp9gu/
        preserveObjectStacking: true,
        stopContextMenu: true,
        fireRightClick: true
    });

    //Affiche une fenêtre modale à l'utilisateur pour le prévenir qu'il faut choisir l'échelle manuellement
    if (isNewProject) {
        $('#calculate-scale-modal').modal('show');
        //Empeche la touche échap de fermer le modal
        $('#calculate-scale-modal').off('keydown');
        //Empeche le clic extérieur de fermer le modal
        $('#calculate-scale-modal').off('click');
    }

    //Empeche de bouger, changer de taille, rotationner une sélection d'objet du canvas si un des objets de la sélection possède ces propriétés
    //Ce qui empêche les cotations d'êtres bougés si on les sélectionne tous ensemble
    //https://jsfiddle.net/Lun395qv/
    canvas.on('selection:created', ({ selected, target }) => {
        if (selected.some(obj => obj.lockMovementX)) {
            target.lockMovementX = true;
        }
        if (selected.some(obj => obj.lockMovementY)) {
            target.lockMovementY = true;
        }
        if (selected.some(obj => obj.lockMovementY)) {
            target.lockScalingY = true;
        }
        if (selected.some(obj => obj.lockMovementX)) {
            target.lockScalingX = true;
        }
        if (selected.some(obj => obj.lockMovementY)) {
            target.lockUniScaling = true;
        }
        if (selected.some(obj => obj.lockMovementY)) {
            target.lockRotation = true;
        }
    })

    CanvasSelection = {
        //Fonction permettant de rendre tous les objects du canvas insélectionnables
        makeEachObjectUnselectable: function() {
            canvas.forEachObject(function(object) {
                object.selectable = false;
            });
        },
        //Fonction permettant de rendre tous les objects du canvas sélectionnables
        makeEachObjectSelectable: function() {
            canvas.forEachObject(function(object) {
                object.selectable = true;
            });
        }
    }
}



//Fonction lancée après validation du modal
function validateScale() {
    //Enlève le modal que si une valeur est rentrée
    if ($("#scaleRealDistance").val()) {
        $('#calculate-scale-modal').modal('hide');
    }
    Scale.activateScaleCalculation();
}

//Distance réelle entre les deux points cliqués lors du calcul de l'échelle
$("#scaleRealDistance").on('input', function() {
    try {
        scaleRealDistance = $("#scaleRealDistance").val();
        scaleRealDistance = Number(scaleRealDistance);
        if (scaleRealDistance == null || scaleRealDistance < 0 || (isNaN(scaleRealDistance))) {
            scaleRealDistance = Number(1);
        }
    } catch (error) {
        scaleRealDistance = Number(1);
    }
    //console.log(scaleRealDistance);
    if (scaleRealDistance != 1) {
        $("#scaleRealDistance").val(scaleRealDistance);
    }
});

function setScaleRealDistance(value) {
    scaleRealDistance = value;
}

function setScaleDistance(value) {
    scaleDistance = value;
}



$('#color-picker').spectrum({
    type: "color",
    hideAfterPaletteSelect: "true",
    showInput: "true",
    showInitial: "true",
    showAlpha: "false"
});

function getCurrentColor() {
    var color = $('#color-picker').val();
    return 'rgba(' + parseInt(color.slice(-6, -4), 16) + ',' + parseInt(color.slice(-4, -2), 16) + ',' + parseInt(color.slice(-2), 16) + ',1)';
}

function getCurrentColorLight() {
    var color = $('#color-picker').val();
    return 'rgba(' + parseInt(color.slice(-6, -4), 16) + ',' + parseInt(color.slice(-4, -2), 16) + ',' + parseInt(color.slice(-2), 16) + ',0.4)';
}

function colorPickerEventHandle() {
    if ($('#colorSection').hasClass("active")) {
        $('#colorSection').hide();
        $('#colorSection').removeClass("active");
    } else {
        $('#colorSection').show();
        $('#colorSection').addClass("active");
    }
}


/**
 * Permet de vérifier si un outil n'est pas en cours d'utilisation
 * pour éviter d'en activer un autre en même temps.
 */
function startUsingTools() {
    if (!isCotationActivated && !isTextActivated && !isPochageActivated && !isClotureActivated && !isMoveMapActivate && !isScaleCalculationActivated) {
        return true;
    } else if (!runningTool) {
        if (isCotationActivated) Cotation.switchActivateDeactivateCotation();
        if (isTextActivated) Text.switchActivateDeactivateText();
        if (isPochageActivated) Pochage.switchActivateDeactivatePochage();
        if (isClotureActivated) Cloture.switchActivateDeactivateCloture();
        if (isMoveMapActivate) MoveMap.clicked();

        return true;
    } else {
        return false;
    }
}