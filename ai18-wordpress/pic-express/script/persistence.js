// AI18 >>>>>>>>>>>>>>>>>>>>>>>>

function getMapPath() {
    return current_project[1].plan;
}

function getCanvasBackup() {
    if (current_project[1].canvasJSON) {
        return JSON.parse(current_project[1].canvasJSON);
    }
    return false;
}

async function saveToDisk(mapPath, canvasBackup) {
    current_project[1].canvasJSON = canvasBackup;
    await update_project(current_project);
    // console.log(canvasBackup);
}
// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<