var mousedowncotation;
var mouseupcotation;
var mousemovecotation;

//Distance entre la cotation et le texte
var dt = 16;
//Taille du texte
var textSize = 12;

var Cotation = 
{
	//Active la cotation
	activate:function(){
		
		//Empeche la sélection pendant la cotation (Cadre de sélection)
		canvas.selection = false;
		//Rend tous les objets du canvas non sélectionnables
		CanvasSelection.makeEachObjectUnselectable();

		var line, line2, line3;
		var arr = new Array();
		var startx = new Array();
		var endx = new Array();
		var starty = new Array();
		var endy = new Array();
		var temp = 0;
		var nbClics = "0";
        var text;
		var isDown = false;
		var px;

		//On retient quand le clic de souris est actif
		canvas.on('mouse:down', mousedowncotation = function(o){
			isDown = true;
		});
				 
		//Quand on le clic de souris est relevé		
		canvas.on('mouse:up', mouseupcotation = function(o){
			if(isDown){
				
				//Le clic de souris n'est plus actif
				isDown = false;
				
				//On incrémente le nombre de clic de la souris
				if(nbClics=="3"){
					nbClics = "0";
				}
				
				//On incrémente le nombre de clic de la souris
				if(nbClics=="2"){
					
					//On enlève les lignes pour les replacer sous forme de groupe selectionnable, mais immobile
					//Ce qui permettra de sélectionner la cotation pour la supprimer s'un bloc
					canvas.remove(line);
					canvas.remove(line2);
					canvas.remove(line3);
					//Création du groupe
					var groupCotation = new fabric.Group([ line, line2, line3 ], {
					//Propriétés pour boquer le groupe de tout mouvement / Changement de taille / Rotation						
					lockMovementX: true, lockMovementY: true, lockScalingX: true, loclScalingY: true, lockUniScaling: true, lockRotation: true, hasControls: false
					});
					
					//Continue l'enregistrement des actions dans l'historique de undo et redo
					UndoRedo.resume();
					
					//On ajoute le groupe de ligne au canvas
					canvas.add(groupCotation);
					
					//Met en pause l'enregistrement des actions dans l'historique de undo et redo
					UndoRedo.pause();
					
					//On retire le texte pour le remettre afin que son plan Z soit au dessus de celui du groupe de lignes
					//Ce qui permet de pouvoir sélectionner le texte pour pouvoir le déplacer par la suite
					canvas.remove(text);
					canvas.add(text);
					
					//Continue l'enregistrement des actions dans l'historique de undo et redo
					UndoRedo.resume();
					
					//Rend tous les objets du canvas non sélectionnables
					CanvasSelection.makeEachObjectUnselectable();
					
					nbClics = "3";
				}
				
				//Après que la ligne principale soit crée, donc après clic de souris
				if(nbClics=="1"){
					
					////Création de la ligne 2
					//La ligne commence au point A et est perpendiculaire au segment [AB]
					var points2 = [startx[temp], starty[temp], startx[temp], starty[temp]];
					line2 = new fabric.Line(points2, {
						 strokeWidth: 1,			
						 stroke: 'red',
						 originX: 'center',
						 originY: 'center',
						 selectable: false	
					 });
					 canvas.add(line2);
					 
					 ////Création de la ligne 3
					 //La ligne commence au point B et est perpendiculaire au segment [AB]
					var points3 = [endx[temp], endy[temp], endx[temp], endy[temp]];
					line3 = new fabric.Line(points3, {
						 strokeWidth: 1,			
						 stroke: 'red',
						 originX: 'center',
						 originY: 'center',
						 selectable: false					 
					 });
					 canvas.add(line3);
					
					//On incrémente le nombre de clic de la souris
					nbClics = "2";
				}
			
				//Première fois où l'on a cliqué, on crée la ligne et le texte
				if(nbClics=="0"){
					
					//Met en pause l'enregistrement des actions dans l'historique de undo et redo
					UndoRedo.pause();
					
					 //On récupère le pointeur de souris
					 var pointer = canvas.getPointer(o.e);
					 //Coordonnées des points du pointeur (le point de départ et d'arrivée de la ligne est le même car la ligne n'est pas encore tracée)
					 var points = [pointer.x, pointer.y, pointer.x, pointer.y];
					 //On stocke les coordonnées du 1er clic de souris
					 startx[temp] = pointer.x;
					 starty[temp] = pointer.y;
					 //On crée la ligne
					 line = new fabric.Line(points, {
						 strokeWidth: 2,			
						 stroke: 'red',
						 originX: 'center',
						 originY: 'center',
						 selectable: false
					 });
					 //On ajoute la ligne au canvas
					 canvas.add(line);
					 
					 ////Création du texte	
					 //On récupère les coordonnées de la souris
					 endx[temp] = pointer.x;
					 endy[temp] = pointer.y;	
					 //On calcule la distance entre les points A et B (entre le 1er et le deuxième clic de souris)
					 var px = Calculate.lineLength(startx[temp], starty[temp], endx[temp], endy[temp]).toFixed(2);	
					 //Coordonnées de A
					 var xa = startx[temp];
					 var ya = starty[temp];
					 //Cordonnées de B
					 var xb = endx[temp];
					 var yb = endy[temp];
					 //Calcul du milieu du segment [AB]
					 var middleX = xa + ((xb-xa)/2);
					 var middleY = ya + ((yb-ya)/2);			 
					 //On crée le texte qui affiche la distance entre les deux clics
					 text = new fabric.Text(px, { left: middleX, top: middleY, fontSize: textSize, originX: "center", originY: "center", fontWeight: "bold"});
					 //On ajoute le texte au canvas
					 canvas.add(text);
					 
					 //On incrémente le nombre de clic de la souris
					 nbClics = "1"
				}	
				
				
				
			}
		});

		//Quand la souris bouge
		canvas.on('mouse:move', mousemovecotation = function(o){
			
			//Mise à jour du canvas
            canvas.renderAll();
			
			//Si le nombre de clic vaut 1
			if(nbClics=="1"){
				//Enleve le texte pour éviter qu'il se répète à l'infini
				canvas.remove(text);
				
				//On récupère le pointeur de la souris
				var pointer = canvas.getPointer(o.e);
				
				//Fixe le point d'arrivée de la ligne jusque là où se trouve la souris
				line.set({ x2: pointer.x, y2: pointer.y });
				
				//Prend les coordonnées de la souris
				endx[temp] = pointer.x;
				endy[temp] = pointer.y;		
				
				//Calcul la distance entre les deux points du segment
				var px = Calculate.lineLength(startx[temp], starty[temp], endx[temp], endy[temp]).toFixed(2);
				
				//Coordonnées du point A et B
				var xa = startx[temp];
				var ya = starty[temp];
				var xb = endx[temp];
				var yb = endy[temp];
				
				//Calcul des coordonnées du milieu du segment
				var middleX = xa + ((xb-xa)/2);
				var middleY = ya + ((yb-ya)/2); 
				
				//Variables des coordonnées du texte
				var xt;
				var yt;
				
				//Conditions pour positionner le texte
				//Cas 1 = La pente du segment [AB] est nulle
				if(yb-ya==0){
					xt = middleX;
					yt = middleY + dt;
				} 	
				//Cas 2 = La pente du segment [AB] est infinie
				else if(xb-xa==0){
					xt = middleX + dt;
					yt = middleY;
				}
				//Cas 3 = La pente du segment [AB] est quantifiable
				else{
					//Calculs des pentes
					//Pente de la droite (AB)
					var a1 = (yb-ya)/(xb-xa);
					//Pente de la perpendiculaire à (AB)
					var a2 = (-1)/a1;
					
					//Variables permettant de calculer les coordonnées du texte pour qu'il soit a la distance tx du segement [AB]
					var b = middleX;
					var c = a2;
					var d = dt;
					var e = middleY - a2 * middleX;
					var f = middleY;
					
					//Calculs des cordonnées du texte
					xt = (b-e*c+c*f+Math.sqrt(d*d*c*c-b*b*c*c+2*b*c*f-2*e*b*c+d*d+2*e*f-f*f-e*e))/(1+c*c);
					yt = a2*xt+e;
				}
				
				//Déplacement du texte au milieu du segment
				text.left = xt;
				text.top = yt;
				text.text = px;
				
				//Calcul de l'angle du segment = sert pour le débogage
				//alpha = Math.acos((xb-xa)/(Math.sqrt(Math.pow((xb-xa),2)+Math.pow((yb-ya),2))))*(180/Math.PI);
				//console.log(alpha);
				
				//On ajoute le texte au canvas
				canvas.add(text);				
				
			}
			
			//Si le nombre de clic vaut 2
			if(nbClics=="2"){
				
				//Nouveaux points = projections des points A et B
				var nxa, nxb, nya, nyb;
				
				//Calculs des coordonnées
				var pointer = canvas.getPointer(o.e);
				
				//Initialisation des points
				//Points du 1er clic
				var xa = startx[temp];
				var ya = starty[temp];
				//Points du 2e clic
				var xb = endx[temp];
				var yb = endy[temp];
				//Position de la souris
				var xs = pointer.x;
				var ys = pointer.y;
				
				////Calcul des coordonnées des projections de A et B
				//Cas 1 = La pente du segment [AB] est nulle
				if(yb-ya==0){
					nxa = xa;
					nya = ya+(ys-ya);
					nxb = xb;
					nyb = yb+(ys-yb);
				} 	
				//Cas 2 = La pente du segment [AB] est infinie
				else if(xb-xa==0){
					nxa = xa+(xs-xa);
					nya = ya;
					nxb = xb+(xs-xb);
					nyb = yb;
				}
				//Cas 3 = La pente du segment [AB] est quantifiable
				else{
				
					//Calculs des pentes
					//Pente de la droite (AB)
					var a1 = (yb-ya)/(xb-xa);
					
					//Pente de la perpendiculaire à (AB)
					var a2 = (-1)/a1;
					
					//Calcul de la projection du point A
					nxa = ((ys-a1*xs)-(ya-a2*xa))/(a2-a1);
					nya = a1*nxa+(ys-a1*xs);
					
					//Calcul de la projection du point B
					nxb = ((ys-a1*xs)-(yb-a2*xb))/(a2-a1);
					nyb = a1*nxb+(ys-a1*xs);
				
				}
				
				//Déplacement de la ligne 1 (segment projeté)
				line.set({ x1: nxa, y1: nya, x2: nxb, y2: nyb});
				//Déplacement de la ligne 2 (segment perpendiculaire à la projection et passant par A)
				line2.set({x2: nxa, y2: nya});
				//Déplacement de la ligne 3 (segment perpendiculaire à la projection et passant par B)
				line3.set({x2: nxb, y2: nyb});
				
				//Déplacment du texte (au milieu du segment projeté)
				var middleX = nxa + ((nxb-nxa)/2);
				var middleY = nya + ((nyb-nya)/2);
				
				//Variables des coordonnées du texte
				var xt;
				var yt;
				
				//Conditions pour positionner le texte
				//Cas 1 = La pente du segment [AB] est nulle
				if(yb-ya==0){
					xt = middleX;
					yt = middleY + dt;
				} 	
				//Cas 2 = La pente du segment [AB] est infinie
				else if(xb-xa==0){
					xt = middleX + dt;
					yt = middleY;
				}
				//Cas 3 = La pente du segment [AB] est quantifiable
				else{
					//Calculs des pentes
					//Pente de la droite (AB)
					var a1 = (yb-ya)/(xb-xa);
					//Pente de la perpendiculaire à (AB)
					var a2 = (-1)/a1;
					
					//Variables permettant de calculer les coordonnées du texte pour qu'il soit a la distance tx du segement [AB]
					var b = middleX;
					var c = a2;
					var d = dt;
					var e = middleY - a2 * middleX;
					var f = middleY;
					
					//Calculs des cordonnées du texte
					xt = (b-e*c+c*f+Math.sqrt(d*d*c*c-b*b*c*c+2*b*c*f-2*e*b*c+d*d+2*e*f-f*f-e*e))/(1+c*c)
					yt = a2*xt+e;
				}
				
				//On enleve le texte pour le remettre après, sinon la zone de sélection reste sur place
				canvas.remove(text);
				
				//Déplacement du texte au milieu du segment
				text.left = xt;
				text.top = yt;

				//Ajout du texte au canvas
				canvas.add(text);
				
			}
			
			//Mise à jour du canvas
			canvas.renderAll();
		});	
		
	}
	
	//Désactive la cotation
	,deactivate:function(){
		
		//Rend la multisélection du canvas possible
		canvas.selection = true;
		//Rend tous les objets du canvas non sélectionnables
		CanvasSelection.makeEachObjectSelectable();
		canvas.off('mouse:down', mousedowncotation);
		canvas.off('mouse:up', mouseupcotation);
		canvas.off('mouse:move', mousemovecotation);
		
	}
	
	//Alterne entre l'activation et la désactivation de la cotation sur le canvas
	,switchActivateDeactivateCotation:function(){
			if(isCotationActivated){
				$("#cotationBtn").removeClass("selected");
				Cotation.deactivate();
				isCotationActivated = false;
			}else if (startUsingTools()) {
				$("#cotationBtn").addClass("selected");
				Cotation.activate();
				isCotationActivated = true;
			}
	}
		
}

//Permet de calculer la distance entre deux points
var Calculate={
	lineLength:function(x1, y1, x2, y2){
	var scale = Scale.getScale();
	try {
		if(scale==null || scale<=0 || (isNaN(scale))){
			scale = Number(1);
		}
		}
		catch(error) {
		scale = Number(1);
		}
		return Math.sqrt(Math.pow(x2*1-x1*1, 2)+Math.pow(y2*1-y1*1, 2))*scale;
	}
}	