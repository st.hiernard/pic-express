
var MoveMap = {
    clicked:function () {
        if(isMoveMapActivate){
            $("#moveMapBtn").removeClass("selected");
            $("#mapAndCanvas-container").draggable('destroy');
            isMoveMapActivate = false;
            canvas.selection = true;
        }else if (startUsingTools() || isScaleCalculationActivated) {
            canvas.selection = false;
            $("#moveMapBtn").addClass("selected");
            $( function() {
                $("#mapAndCanvas-container").draggable( {
                    zIndex: 1,
                    scroll: false,
                    start: function( event, ui ) {
                        // Pour annuler la détection de l'event "mouse:down" dans Scale.activateScaleCalculation()
                        scaleMouseLeftClickIsDown = false;
                    }
                });
            } );
            isMoveMapActivate = true;
            
        }
    }
}